package com.practice.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;


import com.practice.jpa.entity.Users;
import com.practice.jpa.repository.UsersRepository;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class DemoApplicationTests{

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	UsersRepository userRepo;

	@Autowired
	EntityManager em;

	@Test
	@Transactional
	public void savetest() {
		Users user = userRepo.save(new Users("Raden@gmail.com", "Raden", "Wijaya", "lagitu"));
		logger.info("new user id -> {}", user.getId());

	}

	@Test
	@Transactional
	public void native_queries_basic() {

		Query query = em.createNativeQuery("SELECT * FROM USERS", Users.class);
		List<Users> resultList = query.getResultList();
		logger.info("SELECT * FROM COURSE -> {}", resultList);
	}

	@Test
	@Transactional
	public void JoinTest(){
		Query query = em.createNativeQuery("SELECT * FROM USERS INNER JOIN USER_PROFILES ON USERS.USER_PROFILES_ID = USER_PROFILES.ID", Users.class);
		List<Object[]> resultList = query.getResultList();
		logger.info("Join test - > {}", resultList);
	}
}
