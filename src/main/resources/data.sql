insert into user_profiles(id, address1, address2, city, country, dob, gender, phone_number)
values(200L, 'jl manggo', 'jl ahmad yani', 'Kendari', 'indonesia', sysdate(), 'pria', '0812345566' );
insert into user_profiles(id, address1, address2, city, country, dob, gender, phone_number)
values(201L, 'jl nanas', 'jl citayem', 'Purwokerto', 'indonesia', sysdate(), 'wanita', '081273324' );
insert into user_profiles(id, address1, address2, city, country, dob, gender, phone_number)
values(202L, 'jl durian', 'jl simatupang', 'Klaten', 'indonesia', sysdate(), 'pria', '083634535' );

insert into users(id, email, first_name, last_name, password, user_profiles_id)
values(100L, 'zainudinsaputra94@gmail.com', 'Zainudin', 'Saputra', 'putrabetawi', 200L);
insert into users(id, email, first_name, last_name, password, user_profiles_id)
values(101L, 'widanurhasan@gmail.com', 'Wida', 'Nur Hasan', 'koplo', 202L);
insert into users(id, email, first_name, last_name, password, user_profiles_id)
values(102L, 'ajeng@gmail.com', 'Ajeng', 'Diah', 'apalah', 201L);
