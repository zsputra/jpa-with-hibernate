package com.practice.jpa.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Entity
public class UserProfiles {
    
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @NotNull
    private String address1;

   
    @NotEmpty
    @NotNull
    private String address2;

    @NotEmpty
    @NotNull
    private String city;


    @NotEmpty
    @NotNull
    private String country;

    @NotEmpty
    @NotNull
    private Date dob;

    @NotEmpty
    @Pattern(regexp = "pria|wanita")
    private String gender;

    @NotEmpty
    @NotNull
    private String phoneNumber;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "userProfiles")
    Users users;

    protected UserProfiles(){}


    public UserProfiles(String address1, String address2, String city, String country, Date dob, String gender, String phoneNumber) {
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.country = country;
        this.dob = dob;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
    }


    public Users getUsers() {
        return this.users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
    

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    




}