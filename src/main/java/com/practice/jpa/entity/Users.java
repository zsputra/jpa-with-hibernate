package com.practice.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;


@Entity
@Component
public class Users {
    
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Size(max=64)
    private String first_name;

    @NotEmpty
    @Size(max=64)
    private String last_name;

    @NotEmpty
    @Size(min = 4, max = 15)
    private String password;

    @OneToOne(fetch = FetchType.LAZY)
    UserProfiles userProfiles;

    protected Users(){

    }


    public Users(String email, String first_name, String last_name, String password) {
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
    }


    public UserProfiles getUserProfiles() {
        return this.userProfiles;
    }

    public void setUserProfiles(UserProfiles userProfiles) {
        this.userProfiles = userProfiles;
    }



    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString(){
        return String.format("first_name - > %s, last_name ", first_name, last_name);
    }
}