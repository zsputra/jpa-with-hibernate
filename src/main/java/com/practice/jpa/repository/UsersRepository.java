package com.practice.jpa.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.practice.jpa.entity.Users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UsersRepository{

    @Autowired
    Users users;

    @Autowired
    EntityManager em;


    //Retrieve user by id
    public Users retrieveUsers(Long id){
        Users user = em.find(Users.class, id);
        return user;
    }

    public Users save(Users user){
        if(user.getId() == null){
            em.persist(user);
        }
        else{
            em.merge(user);
        }
        return user;
    }

    public void joinOperation(){
        TypedQuery query = em.createNamedQuery("get_all_join", Users.class);
        List<Users> user = query.getResultList();
    }

    public List<Object[]> innerJoinOp(){
        Query query = em.createNativeQuery("SELECT * FROM USERS INNER JOIN USER_PROFILES ON USERS.USER_PROFILES_ID = USER_PROFILES.ID", Users.class);
        List<Object[]> resultList = query.getResultList();
        return resultList;
    }

        
    
    
    
}