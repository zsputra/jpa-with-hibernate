package com.practice.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.practice.jpa.entity.UserProfiles;
import com.practice.jpa.entity.Users;
import com.practice.jpa.repository.UsersRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;


@SpringBootApplication
public class DemoApplication implements CommandLineRunner{

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	UsersRepository userRepo;

	@Autowired
	EntityManager em;


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	@Transactional
	public void run(String... arg0) throws Exception{
		Users user = userRepo.retrieveUsers(100L);
		logger.info("User 100L, first name -> {}", user.getFirst_name());
		logger.info("User 100L , last name -> {}", user.getLast_name());

		Users newUser = userRepo.save(new Users("Raden@gmail.com", "Raden", "Wijaya", "lagitu"));
		logger.info("new user id -> {}", newUser.getId());
		logger.info("new user first_name, last_name, email -> {}, {}, {}", newUser.getFirst_name(), newUser.getLast_name(), newUser.getEmail());


		Users user1 = em.find(Users.class, 100L);
		UserProfiles user1Profiles = user1.getUserProfiles();
		logger.info("user1 name -> {}", user1.getFirst_name());
		logger.info("user1 address1 -> {}", user1Profiles.getAddress1());
		logger.info("user1 city -> {}", user1Profiles.getCity());
		logger.info("User1 dob -> {}", user1Profiles.getDob());

		Query query = em.createNativeQuery("SELECT * FROM USERS INNER JOIN USER_PROFILES ON USERS.USER_PROFILES_ID = USER_PROFILES.ID", Users.class);
		List<Object[]> resultList = query.getResultList();
		logger.info("Join test - > {}", resultList);


	}


}


